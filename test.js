// COMET TEST
const WebSocket = require( "ws" );
const ws = new WebSocket( "wss://localhost/" );
var reader = require('readline').createInterface({
    input: process.stdin,
    output: process.stdout
});
reader.on('line', function ( line ) {
	ws.send( line );
});
reader.on('close', function () {});

ws.on( 'message', function( message ) {
	console.log( "[reserved] " + message );
} );
ws.on( 'close', function( code, reason ) {
	console.log( "client closed. [code]" + code + " [reason]" + reason );	
} );
