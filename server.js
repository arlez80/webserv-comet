/*
	Game Server "Comet" by @arlez80 Yui Kinomoto
*/

const express = require( "express" );
const http = require( "http" );
const https = require('https');
const crypto = require('crypto');
const WebSocketServer = require( "ws" ).Server;

const app = express( );
const server = http.createServer( app );

const fs = require("fs");

// ------------------------------------------------
// 設定読み込み
const config = (function( path ) {
	try {
		const src = fs.readFileSync( path );
		return JSON.parse( src );
	}catch( e ) {
		console.log( e );
		console.log( "can't read: " + path );
	}

	return {};
})( "./config.txt" );

console.log( config );

// ------------------------------------------------
// Webhookへのポスト
function post_discord_webhook( url, content )
{
	if( ! url ) return;

	const req = https.request(
		url,
		{
			'method': 'POST',
			'headers': {
				'Content-Type': 'application/json',
			}
		},
		function( res ) {
			// console.log( res );
			console.log(`STATUS: ${res.statusCode}`);
			console.log(`HEADERS: ${JSON.stringify(res.headers)}`);
		}
	);
	req.on('error', function (e) {
		console.error(`request error: ${e.message}`);
	});
	req.write(JSON.stringify({
		"content": content
	}));
	req.end( );
}

// ------------------------------------------------
// WebSocket
const wss = new WebSocketServer({ server });
const Room = require( "./room.js" );
const { join } = require("path");
let rooms = {};

wss.on( 'connection', function( ws, req ) {
	let joined_room = null;
	console.log( "client connected! [time]" + new Date( ).toISOString( ) + " [ip address]" + req.socket.remoteAddress );
	ws.send( "_:T:connected." );

	ws.on( 'message', function( message ) {
		switch( message.charAt( 0 ) ) {
			case "I": {
				// GET ALL ROOMS
				let room_infos = [];
				for( let name in rooms ) {
					let room = rooms[name];
					let room_members = [];
					for( let i in room.clients ) {
						room_members.push( room.clients[i].player_name );
					}
					room_infos.push({
						name: name,
						member_count: room.clients.length,
						max_member_count: room.max_member_count,
						room_members: room_members,
						locked: room.locked,
						password: room.password != "",
						attributes: room.attributes
					});
				}
				ws.send( "_:I:" + JSON.stringify( room_infos ) );
			}	break;
			case "J": {
				// JOIN
				if( joined_room ) {
					ws.send( "_:E:You are joined room already." )
				}else {
					let room_name = "";
					let password = "";
					let attributes = [];
					let max_member_count = 16;
					let player_name = "";
					// 部屋名前取得
					if( message.substr( 2 )[0] == "{" ) {
						// 新版
						const json_result = JSON.parse( message.substr( 2 ) );
						room_name = json_result.room_name;
						password = json_result.password;
						attributes = json_result.attributes;
						player_name = json_result.player_name;
						if( json_result.max_member_count ) {
							max_member_count = json_result.max_member_count;
						}
					}else {
						// 旧版
						room_name = message.substr( 2 );
					}
					if( rooms[room_name] ) {
						// 入室
						joined_room = rooms[room_name];
						if( joined_room.password != "" ) {
							const shasum = crypto.createHash('sha1');
							shasum.update( password );
							if( joined_room.password != shasum.digest('hex') ) {
								joined_room = null;
							}
						}
					}else {
						// 新規作成
						joined_room = new Room( room_name );
						rooms[room_name] = joined_room;
						if( password != "" ) {
							const shasum = crypto.createHash('sha1');
							shasum.update( password );
							joined_room.password = shasum.digest('hex');
						}
						joined_room.attributes = attributes;
						joined_room.max_member_count = max_member_count;

						// 新規作成時に、パスワードがなければポストする
						if( password == "" ) {
							try {
								if( config.notice.room.created ) {
									post_discord_webhook( config.notice.room.created, "部屋 " + room_name + " が作成されました。" );
								}
							}catch( e ) {
								// にぎりつぶす
								console.log( "CANNOT SEND to DISCORD" )
							}
						}
					}
					if( joined_room && joined_room.join( ws, player_name ) ) {
						// succeed
					}else if( joined_room == null ) {
						ws.send( "_:E:Wrong password to join room." );
					}else {
						ws.send( "_:E:Failure to join room." );
						joined_room = null;
					}
				}
			}	break;
			case "L": {
				// LEAVE
				if( joined_room ) {
					joined_room.leave( ws );
					if( joined_room.clients.length == 0 ) {
						delete rooms[joined_room.name];
					}
				}else {
					ws.send( "_:E:You doesn't joined room." );
				}
				joined_room = null;
			}	break;
			case "E": {
				// ERROR
				console.warn( "!! client sends ERROR command !!" );
			}	break;
			case "K": {
				// KICK
				console.warn( "!! client sends KICK command !!" );
			}	break;
			case "O": {
				// LOCK
				if( joined_room ) {
					joined_room.locked = true;
				}else {
					ws.send( "_:E:You doesn't joined room." );
				}
			}	break;
			case "U": {
				// UNLOCK
				if( joined_room ) {
					joined_room.locked = false;
				}else {
					ws.send( "_:E:You doesn't joined room." );
				}
			}	break;
			case "S": {
				// SEND DATA
				if( joined_room ) {
					joined_room.send( joined_room.get_client_id( ws ) + ":" + message, joined_room.get_client_id( ws ) );
				}else {
					ws.send( "_:E:You doesn't joined room." );
				}
			}	break;
			case "A": {
				// ASK OWN STATUS
				if( joined_room ) {
					var members = [];
					for( var i in joined_room.clients ) {
						members.push( joined_room.clients[i].id );
					}
					ws.send(
						"_:A:"
					+	JSON.stringify({
						id: joined_room.get_client_id( ws )
					,	name: joined_room.name
					,	members: members
					})
					);
				}else {
					ws.send( "_:E:You doesn't joined room." );
				}
			}	break;
			case "T": {
				// TEST
				ws.send( "_:" + message );
			}	break;
			default: {
				// Unknown
				const cmd = message.charAt( 0 );
				console.warn( "!! client sends unknown " + cmd + " command !!" );
				ws.send( "_:E:" + cmd + " is unknown command." );
			}	break;
		}
	});

	ws.on( 'close', function( code, reason ) {
		console.log( "client closed. [code]" + code + " [reason]" + reason + " [time]" + new Date( ).toISOString( ) + " [ip address]" + req.socket.remoteAddress );

		if( joined_room ) {
			joined_room.leave( ws );
			if( joined_room.clients.length == 0 ) {
				delete rooms[joined_room.name];
			}
		}
		joined_room = null;
	});
	ws.on( 'error', function( reason ) {
		console.log( "client error occured. [reason]" + reason + " [time]" + new Date( ).toISOString( ) + " [ip address]" + req.socket.remoteAddress );

		if( joined_room ) {
			joined_room.leave( ws );
			if( joined_room.clients.length == 0 ) {
				delete rooms[joined_room.name];
			}
		}
		joined_room = null;
	});
});

// ------------------------------------------------
// normal pages
app.use( express.static( "public" ) );

// listen for requests :)
const listener = server.listen( process.env.PORT || 51556, function( ) {
	console.log( "Your app is listening on port " + listener.address( ).port );
});

app.get( "/", function( request, response ) {
	response.send( String( listener.address( ).port ) );
});
  
