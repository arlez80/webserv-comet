/*
	Game Server "Comet" Room by @arlez80 Yui Kinomoto
*/

module.exports = class Room {
	constructor ( name ) {
		this.name = name;
		this.id = 1;
		this.locked = false;
		this.max_member_count = 16;
		this.password = "";
		this.clients = [];
		this.attributes = [];
	}

	join( new_ws, player_name ) {
		if( this.locked || ( this.max_member_count <= this.clients.length ) ) {
			return false;
		}

		const new_client_id = this.id;
		const join_message = "_:J:" + new_client_id;
		this.clients.push({ id: new_client_id, ws: new_ws, name: player_name });
		this.id ++;

		this.send( join_message );

		return true;
	}

	get_client_id( ws ) {
		for( let i in this.clients ) {
			if( this.clients[i].ws == ws ) {
				return this.clients[i].id;
			}
		}

		return null;
	}

	send( message, exclude_id ) {
		if( ! exclude_id ) {
			exclude_id = null;
		}

		for( let client in this.clients ) {
			if( exclude_id != this.clients[client].id ) {
				this.clients[client].ws.send( message );
			}else {
				// doesn't send
			}
		}
	}

	leave( ws ) {
		for( let i in this.clients ) {
			const client = this.clients[i];
			if( client.ws == ws ) {
				this.send( "_:L:" + client.id );
				this.clients.splice( i, 1 );
				return;
			}
		}
	}
};
